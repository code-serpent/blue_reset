#!/usr/bin/env bash
#Vist the link below for more infomation. 
#https://askubuntu.com/questions/71863/how-to-change-pulseaudio-sink-with-pacmd-set-default-sink-during-playback/72076#72076
#Map this to a keyboard shortcut for ease of use. 

#Prerequirment - Allow us to modify the output device via the CLI. 
#Run the below line. It only needs to be run the first time. (Just uncomment on the first run) 
#sed -i 's/load-module module-stream-restore.*/load-module module-stream-restore restore_device=false/' /etc/pulse/default.pa

#VARS
BLUE_SINK_NAME=`pacmd list-sinks | grep -i "bluez_sink" | awk -F ": <" {'print($2)'} | sed 's/>//'`
BLUE_CARD_NAME=`pactl list | grep -i "bluez_card" | awk -F ': ' '{print($2)}'`  
LIST_OF_INPUTS=`pacmd list-sink-inputs | grep -i index | cut -d":" -f2`

#Toggles audio profile of BLUETooth Audio Card
pactl set-card-profile $BLUE_CARD_NAME headset_head_unit
pactl set-card-profile $BLUE_CARD_NAME a2dp_sink

#VARS needs to be called after the profile switch
INDEX_FOR_HEADSET=`pacmd list-sinks | grep -i index | cut -d":" -f2 | tail -n 1`


#Move the audio stream to the headset. This doesn't happen for audio streams that existed
#before the profile switch. All new audio streams will attach to the default sink 
for input in $LIST_OF_INPUTS; do pacmd move-sink-input $input $INDEX_FOR_HEADSET; done

#Sets the BLUEThooth audio sink as the defualt output devices
pacmd set-default-sink $BLUE_SINK_NAME

exit 0
